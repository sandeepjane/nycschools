package com.example.nycschools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nycschools.data.ApiService
import com.example.nycschools.data.MainRepository
import com.example.nycschools.data.School
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MainRepositoryTest {

    @get:Rule
    val instantTaskExecutionRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var apiService: ApiService


    private lateinit var mainRepository: MainRepository

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        mainRepository = MainRepository(apiService)
    }


    @Test
    fun getNewYorkSchoolsTest() {
        runBlocking {
            val schoolList = listOf<School>()
            Mockito.`when`(apiService.getNewYorkCitySchools())
                .thenReturn(listOf<School>())
            val response = mainRepository.getNewYorkSchools()
            assertEquals(schoolList, response)
        }
    }
}