package com.example.nycschools.ui.home

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.data.MainRepository
import com.example.nycschools.data.School
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val mainRepository: MainRepository) : ViewModel() {

    var schoolList = mutableStateOf<List<School>>(listOf())

    fun getNewYorkSchools() {
        viewModelScope.launch {
            kotlin.runCatching {
                mainRepository.getNewYorkSchools()
            }.onSuccess {
                schoolList.value = it
            }.onFailure {

            }
        }
    }

}