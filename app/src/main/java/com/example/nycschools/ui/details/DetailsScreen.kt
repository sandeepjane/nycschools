package com.example.nycschools.ui.details

import android.graphics.fonts.FontStyle
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.nycschools.data.School

@Composable
fun DetailsScreen(school: School, onBackClick:() -> Unit = {}) {
    Column(Modifier.fillMaxSize()) {
        ToolbarWithBackButton(title = school.school_name.orEmpty(), onBackClick)
        LazyColumn(modifier = Modifier.padding(12.dp)) {
            item {
                Spacer(modifier = Modifier.height(12.dp))
                Text(text = "Overview", style = MaterialTheme.typography.titleLarge)
                Spacer(modifier = Modifier.height(12.dp))
                Text(text = school.overview_paragraph.orEmpty())
                Divider(Modifier.padding(24.dp))
                Text(text = "Contact", style = MaterialTheme.typography.titleLarge)
                Spacer(modifier = Modifier.height(12.dp))
                Text(text = school.phone_number.orEmpty())
                Spacer(modifier = Modifier.height(12.dp))
                Text(text = school.school_email.orEmpty(), textDecoration = TextDecoration.Underline)
                Spacer(modifier = Modifier.height(12.dp))
                Text(text = school.website.orEmpty(), textDecoration = TextDecoration.Underline)
                Spacer(modifier = Modifier.height(12.dp))
                Text(text = school.primary_address_line_1.orEmpty())
                Text(text = school.city.orEmpty())
                Text(text = school.state_code.orEmpty())
                Divider(Modifier.padding(24.dp))
            }
        }
    }

}


@Composable
fun ToolbarWithBackButton(title: String, onBackClick:() -> Unit = {}) {
    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(horizontal = 12.dp), verticalAlignment = Alignment.CenterVertically) {
        Image(imageVector = Icons.Default.ArrowBack, contentDescription = "back icon", modifier = Modifier
            .size(32.dp)
            .clickable {
                onBackClick()
            })

        Text(
            text = title, style = MaterialTheme.typography.headlineMedium,
            textAlign = TextAlign.Center, modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        )
    }
}

@Preview
@Composable
fun ToolbarPreview() {
    Surface {
        ToolbarWithBackButton(title = "School Name onBackClick onBackClick")
    }
}