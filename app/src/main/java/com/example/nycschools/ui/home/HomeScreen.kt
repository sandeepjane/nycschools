package com.example.nycschools.ui.home

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.nycschools.data.School

@Composable
fun HomeScreen(
    onNavigateToDetail: (School)-> Unit = {},
    homeViewModel: HomeViewModel = hiltViewModel()
) {
    val schoolList = homeViewModel.schoolList.value

    Column(Modifier.fillMaxSize()) {
        Text(
            text = "New York City Schools", style = MaterialTheme.typography.titleLarge,
            textAlign = TextAlign.Center, modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        )

        LazyColumn() {
            items(schoolList) {
                SchoolItem(school = it) {
                    onNavigateToDetail(it)
                }
            }
        }
    }


    LaunchedEffect(key1 = Unit) {
        homeViewModel.getNewYorkSchools()
    }
}


@Composable
fun SchoolItem(school: School, onItemClick: (School) -> Unit = {}) {
    Column(modifier = Modifier.padding(8.dp).clickable {
        onItemClick(school)
    }) {
        Text(text = school.school_name.orEmpty())
        Spacer(modifier = Modifier.height(8.dp))
        Divider()
    }
}

@Preview
@Composable
fun SchoolItemPreview() {
    Surface {
        SchoolItem(school = School("123", "School 1", "", "", "", "", "", "", "", ""))
    }
}