package com.example.nycschools.base

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCApp : Application() {
    override fun onCreate() {
        super.onCreate()
    }
}