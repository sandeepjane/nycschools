package com.example.nycschools.base

import android.net.Uri
import android.os.Bundle
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.nycschools.data.School
import com.example.nycschools.ui.details.DetailsScreen
import com.example.nycschools.ui.home.HomeScreen
import com.google.gson.Gson

@Composable
fun AppNavState(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
    startDestination: String = Screens.home
) {
    NavHost(modifier = modifier,navController = navController,
        startDestination = startDestination) {
        composable(Screens.home) {
            HomeScreen(onNavigateToDetail = {
                val json = Uri.encode(Gson().toJson(it))
                navController.navigate("details/$json")
            })
        }
        composable(Screens.details, arguments = listOf(navArgument("school"){
            type = SchoolParamType()
        })) {
            val school = it.arguments?.getParcelable<School>("school")
            if (school != null) {
                DetailsScreen(school = school) {
                    navController.navigateUp()
                }
            }
        }
    }
}
class SchoolParamType : NavType<School>(isNullableAllowed = true) {

    override fun get(bundle: Bundle, key: String): School? {
        return bundle.getParcelable(key)
    }

    override fun parseValue(value: String): School {
        return Gson().fromJson(value, School::class.java)
    }

    override fun put(bundle: Bundle, key: String, value: School) {
        bundle.putParcelable(key, value)
    }
}



object Screens {
    const val home = "Home"
    const val details = "Details/{school}"
}