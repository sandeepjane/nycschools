package com.example.nycschools.data

import com.example.nycschools.data.ApiService
import javax.inject.Inject

class MainRepository @Inject constructor(private val apiService: ApiService) {

    suspend fun getNewYorkSchools() = apiService.getNewYorkCitySchools()

}