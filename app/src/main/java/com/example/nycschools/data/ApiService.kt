package com.example.nycschools.data

import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("resource/s3k6-pzi2.json")
    suspend fun getNewYorkCitySchools() : List<School>
}