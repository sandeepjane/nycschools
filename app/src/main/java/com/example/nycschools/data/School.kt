package com.example.nycschools.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class School (val dbn: String?,
                   val school_name : String?,
                   val overview_paragraph : String?,
                   val phone_number: String?,
                   val school_email: String?,
                   val website: String?,
                   val primary_address_line_1: String?,
                   val city: String?,
                   val state_code: String?,
                   val zip: String?) : Parcelable