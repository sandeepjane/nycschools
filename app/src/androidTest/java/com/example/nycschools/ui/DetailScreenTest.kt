package com.example.nycschools.ui

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import com.example.nycschools.ui.details.DetailsScreen
import org.junit.Rule
import org.junit.Test

class DetailScreenTest {
    @get: Rule(order = 1)
    val composeTestRule = createComposeRule()

    @Test
    fun schoolDetailsTitleTest() {
        composeTestRule.setContent {
            DetailsScreen(school = mockSchool)
        }
        composeTestRule.onNodeWithText("School Name").assertIsDisplayed()
        composeTestRule.onNodeWithText("1234").assertIsDisplayed()
    }
}