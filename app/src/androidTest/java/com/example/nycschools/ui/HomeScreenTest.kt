package com.example.nycschools.ui

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.nycschools.data.School
import com.example.nycschools.ui.home.HomeScreen
import com.example.nycschools.ui.home.SchoolItem
import com.example.nycschools.ui.theme.NYCSchoolsTheme
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class HomeScreenTest {

    @get: Rule(order = 1)
    val composeTestRule = createComposeRule()

    @Test
    fun homeScreenSchoolDisplayTest() {
        composeTestRule.setContent {
            NYCSchoolsTheme {
                SchoolItem(school = mockSchool)
            }
        }

        composeTestRule.onNodeWithText("School Name").assertIsDisplayed()
    }
}

val mockSchool = School("","School Name","","1234","","","","","","")